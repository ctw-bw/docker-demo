FROM ubuntu:20.04

LABEL maintainer="Robert <robert.soor@gmail.com>"

# Prepare system
RUN apt-get update -q && apt-get install -y sl && \
    ln -s /usr/games/sl /usr/bin/sl

# Prepare train on login
RUN echo "sl" >> ~/.bashrc

# Set-up run entrypoint to regular bash
ENTRYPOINT [ "/bin/bash" ]

