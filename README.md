# Docker Demo

This repository contains the Dockerfile for a steam locomotive container.

## Run Container

To test it, simply do:

```
sudo docker pull be1et/docker-demo:latest
sudo docker run -it --rm be1et/docker-demo:latest
```

